import styles from "./TodoItem.module.css";

export default function TodoItem({ item, deleteTodo, checkTodo }) {
  return (
    <tr className={styles.row}>
      <td>
        <input type="checkbox" onClick={() => checkTodo(item.id)} />
      </td>
      <td
        style={{ width: "290px" }}
        className={item.completed ? styles.completed : ""}
      >
        {item.title}
      </td>
      <td className={styles.content}>
        <button className={styles.button} onClick={() => deleteTodo(item.id)}>
          Delete
        </button>
      </td>
    </tr>
  );
}
