import TodoItem from "./TodoItem";
import styles from "./TodoList.module.css";

export default function TodoList({ data, deleteTodo, editTodo, checkTodo }) {
  return (
    <div className={styles.container}>
      <table style={{ borderCollapse: "collapse" }}>
        <tbody>
          {data?.map((todo) => (
            <TodoItem
              key={todo.id}
              item={todo}
              deleteTodo={deleteTodo}
              editTodo={editTodo}
              checkTodo={checkTodo}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}
