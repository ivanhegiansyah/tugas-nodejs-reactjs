import { useEffect, useState } from "react";

import TodoList from "../../components/Todo/TodoList";
import TodoInput from "../../components/Todo/TodoInput";

export default function Home() {
  const [todoList, setTodoList] = useState(() => {
    const savedTodos = localStorage.getItem("data");
    if (savedTodos) {
      return JSON.parse(savedTodos);
    } else {
      return [
        {
          data: [],
        },
      ];
    }
  });

  const deleteTodo = (id) => {
    const todos = todoList.data.filter((item) => item.id !== id);
    setTodoList({ data: todos });
  };

  const addTodo = (newTodo) => {
    const todo = { id: Math.floor(Math.random() * 10000), ...newTodo };
    setTodoList({ data: [...todoList.data, todo] });
  };

  const checkTodo = (id) => {
    const todos = todoList.data.map((todo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
      return todo;
    });
    setTodoList({ data: todos });
  };

  const updatedTodos = () => {
    const newTodo = JSON.parse(localStorage.getItem("data"));
    setTodoList(newTodo);
  };

  useEffect(() => {
    localStorage.setItem("data", JSON.stringify(todoList));
    updatedTodos();
  }, [todoList]);

  return (
    <div>
      <h1>Todo App</h1>
      <TodoInput addTodo={addTodo} />
      <TodoList
        data={todoList.data}
        deleteTodo={deleteTodo}
        checkTodo={checkTodo}
      />
    </div>
  );
}
